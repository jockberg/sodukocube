import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

public class Solver {

	public boolean solved=false;
	public Long startTime;

	public Cube solve(Cube c) {


		int progresCounter=0;
		while (progresCounter!=c.countZeroes()) {
			progresCounter=c.countZeroes();	
			c.updateHeights();

			if(c.countZeroes()==0) {
				solved=true;
				System.out.println("OHHHH YEAHHH!!!   Time: "+(Long)(System.currentTimeMillis()-startTime)+" ms");
				c.printBase();
				System.exit(0);
				break;
			}
		}
		return c;
	}


	public ArrayList<Cube> paths(ArrayList<Cube> c) {

		ArrayList<Cube>cubes = new ArrayList<>();
		for(Cube cc:c) {
			cc.reziseHeights();
			for(Cube cu: cc.paths()){
				cu=solve(cu);
				if(!cubes.contains(cu)){
				cubes.add(cu);
				//cu.printBase();
				}
			}
		}

		//System.out.println(cubes.size()+" New Cubes Created");
		return cubes;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Solver s = new Solver();
		s.startTime=System.currentTimeMillis();
		int[][]Hardestbrick  = new int[][]{
			{ 8, 0, 0, 0, 0, 0, 0, 0, 0  },
			{ 0, 0, 3, 6, 0, 0, 0, 0, 0, },
			{ 0, 7, 0, 0, 9, 0, 2, 0, 0, },
			{ 0, 5, 0, 0, 0, 7, 0, 0, 0, },
			{ 0, 0, 0, 0, 4, 5, 7, 0, 0, },
			{ 0, 0, 0, 1, 0, 0, 0, 3, 0, },
			{ 0, 0, 1, 0, 0, 0, 0, 6, 8, },
			{ 0, 0, 8, 5, 0, 0, 0, 1, 0, },
			{ 0, 9, 0, 0, 0, 0, 4, 0, 0, },
		};
		
		
		int[][]emptyBrick  = new int[][]{
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0  },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, },
		};


		ArrayList<Cube>nextPaths = new ArrayList<>();
		Cube c = new Cube(Hardestbrick);
		c=s.solve(c);
	
		nextPaths.add(c);
		ArrayList<Cube>All = new ArrayList<>();
		
		All = s.paths(nextPaths);

		for (int i = 0; i < 100; i++) {
			All = s.paths(All);
			System.out.println("iteration: "+i +", cubes to Process:"+All.size());
		}
	}
}