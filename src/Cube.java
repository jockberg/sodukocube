import java.util.ArrayList;
import java.util.Arrays;

public class Cube{
	int[][][] cube = new int[9][9][10];
	private int[][] board;
	boolean valid = true;

	public Cube(Cube cube) {
		this.copyCube(cube);
	}

	public boolean rowContainsNumber(int row, int val) {
		return Arrays.stream(getRow(row)).anyMatch(i -> cube[i.getRow()][i.getColumn()][0]==val);
	}


	public boolean colContainsNumber(int col, int val) {
		return Arrays.stream(getColumn(col)).anyMatch(i -> cube[i.getRow()][i.getColumn()][0]==val);
	}


	public Cordinate[] getColumn(int col) {
		Cordinate[] res = new Cordinate[9];
		for(int i=0;i<9;i++) {
			res[i]=new Cordinate(i,col);
		}
		return res;
	}


	public Cordinate[] getRow(int row) {
		Cordinate[] res = new Cordinate[9];
		for(int i=0;i<9;i++) {
			res[i]=new Cordinate(row,i);
		}
		return res;
	}

	public Cordinate getSquareCordinates(int square) {
		int	clearRow = 5000,clearCol =5000;		
		
		if(square==1) {
			clearRow=0;
			clearCol=0;
		}else if(square==2) {
			clearRow=0;
			clearCol=3;
		}else if(square==3) {
			clearRow=0;
			clearCol=6;
		}else if(square==4) {
			clearRow=3;
			clearCol=0;
		}else if(square==5) {
			clearRow=3;
			clearCol=3;
		}else if(square==6) {
			clearRow=3;
			clearCol=6;
		}else if(square==7) {
			clearRow=6;
			clearCol=0;
		}else if(square==8) {
			clearRow=6;
			clearCol=3;
		}else if(square==9) {
			clearRow=6;
			clearCol=6;
		}

		return new Cordinate(clearRow,clearCol);
	}

	public boolean squareContainsNumber(int square, int val) {

		Cordinate clear=getSquareCordinates(square);

		for (int i = clear.getRow(); i<clear.getRow()+3; i++) {
			for (int j = clear.getColumn(); j < clear.getColumn()+3; j++) {
				if(cube[i][j][0] == val && val!=0) {
					return true;
				}
			}
		}
		return false;
	}

	public Cube(int[][] soduko) {
		this.board=soduko;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k < 10; k++) {
					cube[i][j][k] = k;
				}
			}
		}

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if(soduko[i][j]!=0) {
					numberFoundRemoveRest(i, j, soduko[i][j]);	
				}
			}
		}
	}


	public Cube() {
	}

	public void removeAltfromRow(int row, int col, int val) {

		for (int j = 0; j < 9; j++) {
			for (int k = 0; k<cube[row][j].length; k++) {
				if(cube[row][j][k] == val && val!=0) {
					if(cube[row][j].length>1) {
						cube[row][j][k]=0;
					}
				}
			}
		}
	}

	public void removeAltfromColumn(int row, int col, int val) {

		for (int i = 0; i<9; i++) {
			for (int k = 0; k < cube[i][col].length; k++) {
				if(cube[i][col][k] == val && val!=0) {
					if(cube[i][col].length>1) {
						cube[i][col][k]=0;
					}
				}
			}
		}
	}


	public void removeAltfromSquare(int square, int row, int col, int val) {

		Cordinate clear = getSquareCordinates(square);

		for (int i = clear.getRow(); i<clear.getRow()+3; i++) {
			for (int j = clear.getColumn(); j < clear.getColumn()+3; j++) {
				for (int k = 0; k<cube[i][j].length; k++) {
					if(cube[i][j][k] == val && val!=0) {
						if(cube[i][j].length>1) {
							cube[i][j][k]=0;
						}
					}
				}
			}
		}
	}


	public int getSquare(int row, int col, int val) {

		if(row <3 ) {
			if(col<3) {
				return 1;
			}else if(col>2 && col<6) {
				return 2;
			}else if(col>5 && col<9) {
				return 3;
			}
		}else if(row >2 && row <6) {
			if(col<3) {
				return 4;
			}else if(col>2 && col<6) {

				return 5;
			}else if(col>5 && col<9) {
				return 6;
			}
		}else {
			if(col<3) {
				return 7;
			}else if(col>2 && col<6) {
				return 8;
			}else if(col>5 && col<9) {
				return 9;
			}
		}
		return 0;
	}


	public void numberFoundRemoveRest(int row, int col, int val) {

		if(validate(row,col,val)) {
			removeAltfromRow(row, col, val);
			removeAltfromColumn(row, col, val);
			removeAltfromSquare(getSquare(row, col, val),row,col, val);
			cube[row][col]=new int[] {val};
		}
	}


	public void copyCube(Cube oldCube) {

		this.setBoard(oldCube.getBoard().clone());
		for (int i = 0; i<9; i++) {
			for (int j = 0; j < 9; j++) {
				this.cube[i][j]= oldCube.cube[i][j].clone();
			}
		}
	}

	public void printBase() {

		for (int i = 0; i<9; i++) {
			String res="";
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k<1; k++) {
					res+=cube[i][j][k];
				}
			}
			System.out.println(res);
		}
	}

	public void printSpecificHight(int row, int col) {

		for (int i = 0; i < cube[row][col].length; i++) {
			System.out.println(cube[row][col][i]);
		}
	}


	public void updateHeights() {
		onlyOptionLeft();
		allOtherRowMissesAlternative();
		allOtherinColumnMissesAlternative();
		allOtherInSquareMissesAlternative(); 
	}


	public void onlyOptionLeft() {
		int count=0;
		int index=0;

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k<cube[i][j].length; k++) {
					if(cube[i][j][k]!=0) {
						count++;
						index=k;
					}
				}
				if(count==1 && cube[i][j].length>1) {
					numberFoundRemoveRest(i,j,cube[i][j][index]);
				}
			}
		}
	}


	public void allOtherRowMissesAlternative() {

		for (int value = 1; value < 10; value++) {	

			int rowIndex=0;
			for (int i = 0; i < 9; i++) {
				int colIndex=0;
				int count=0;
				int index=0;
				for (int j = 0; j < 9; j++) {

					for (int k = 0; k<cube[i][j].length; k++) {

						if(cube[i][j][k]==value && cube[i][j].length!=1) {
							count++;
							index=k;
							colIndex=j;
							rowIndex=i;
						}
					}
				}
				if(count==1) {
					numberFoundRemoveRest(rowIndex,colIndex,cube[rowIndex][colIndex][index]);
				}
			}

		}
	}

	public void allOtherinColumnMissesAlternative() {

		for (int value = 1; value < 10; value++) {	
			int rowIndex=0;
			for (int col = 0; col < 9; col++) {
				int colIndex=0;
				int count=0;
				int index=0;
				for (int row = 0; row < 9; row++) {
					for (int k = 0; k<cube[row][col].length; k++) {
						if(cube[row][col][k]==value && cube[row][col].length!=1) {
							count++;
							index=k;
							colIndex=col;
							rowIndex=row;
						}
					}
				}
				if(count==1) {
					numberFoundRemoveRest(rowIndex,colIndex,cube[rowIndex][colIndex][index]);
				}
			}
		}
	}

	public void allOtherInSquareMissesAlternative() {

		for(int square=1;square<10;square++) {
			Cordinate clear = getSquareCordinates(square);
			int colIndex=0;
			int index=0;
			int rowIndex=0;
			for (int value = 1; value < 10; value++) {
				int count=0;
				for (int i = clear.getRow(); i<clear.getRow()+3; i++) {
					for (int j = clear.getColumn(); j < clear.getColumn()+3; j++) {
						for (int k = 0; k<cube[i][j].length; k++) {
							if(cube[i][j][k] == value && cube[i][j].length!=1 && value!=0 ) {
								count++;
								rowIndex=i;
								colIndex=j;
								index=k;
							}
						}
					}
				}
				if(count==1) {
					numberFoundRemoveRest(rowIndex,colIndex,cube[rowIndex][colIndex][index]);
				}
			}
		}
	}

	public int countZeroes() {

		int count=0;
		for (int i = 0; i<9; i++) {
			String res="";
			for (int j = 0; j < 9; j++) {
				for (int k = 0; k<1; k++) {
					if(cube[i][j][k]==0) {
						count++;
					}
				}
			}
		}
		return count;
	}

	public ArrayList<Cube> paths() {

		ArrayList<Cube> paths = new ArrayList<>();
		Cordinate shortest= identifySmallestHeight();

		int height=this.cube[shortest.getRow()][shortest.getColumn()].length;

		for(int i=1;i<height;i++) {
			Cube path = new Cube(this);
			if(cube[shortest.getRow()][shortest.getColumn()][i]!=0) {
				path.numberFoundRemoveRest(shortest.getRow(),shortest.getColumn(),path.cube[shortest.getRow()][shortest.getColumn()][i]);
				paths.add(path);
			}
		}
		return paths;
	}

	private boolean validate(int row, int column, int val) {
		valid=!rowContainsNumber(row,val ) && !colContainsNumber(column, val)  && !squareContainsNumber(getSquare(row, column, val), val);
		return valid;
	}

	public void reziseHeights() {

		for (int i = 0; i<9; i++) {
			for (int j = 0; j < 9; j++) {
				ArrayList<Integer> tempHeight= new ArrayList<>();
				if(cube[i][j].length>1) {
					tempHeight.add(0);
					for (int k = 0; k<cube[i][j].length; k++) {
						if(cube[i][j][k]!=0) {
							tempHeight.add(cube[i][j][k]);
						}
					}
					if(!tempHeight.isEmpty() && tempHeight.size()>1) {
						cube[i][j] = tempHeight.stream().mapToInt(h -> h).toArray();
					}
				}
			}
		}
	}

	public Cordinate identifySmallestHeight() {

		int small=10;
		Cordinate pos = new Cordinate();

		for (int i = 0; i<9; i++) {
			String res="";
			for (int j = 0; j < 9; j++) {

				if(small>cube[i][j].length && cube[i][j].length>2 && cube[i][j][1]!=0) {
					small=cube[i][j].length;
					pos=new Cordinate(i,j);
				}
			}
		}
		return pos;
	}


	public int[][] getBoard() {
		return board;
	}


	public void setBoard(int[][] board) {
		this.board = board;
	}
}