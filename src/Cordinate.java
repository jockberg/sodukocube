
public class Cordinate {

	private int row,column;

	public Cordinate(int row, int col) {
		this.row=row;
		this.column=col;
	}
	public Cordinate() {
		// TODO Auto-generated constructor stub
	}
	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
}
